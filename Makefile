# Makefile for the MimeFilter package
#
# Copyright (C) 2000-2020 Davide Giovanni Maria Salvetti.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# On Debian GNU/Linux System you can find a copy of the GNU General Public
# License in "/usr/share/common-licenses/GPL".

VER = $(shell git tag -l | tail -n 1 | cut -d '/' -f 2)
PACKAGE = mimefilter

INSTALL = install -o root -g root
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA    = $(INSTALL) -m 644
INSTALL_DIR     = $(INSTALL) -d

etcdir = /etc/
prefix = /usr/local/
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
datarootdir = $(prefix)/share
datadir     = $(datarootdir)
mandir      = $(datarootdir)/man
man1dir     = $(mandir)/man1
docdir      = $(datarootdir)/doc/$(PACKAGE)

.PHONY: all
all: build

.PHONY: build
build: $(PACKAGE).1

$(PACKAGE).1: $(PACKAGE).pl
	pod2man --section 1 --center "Debian GNU/Linux Manual" \
		--release "Debian Project" $< > $@

.PHONY: installdirs
installdirs:
	test -d $(DESTDIR)$(bindir) \
		|| $(INSTALL_DIR) $(DESTDIR)$(bindir)
	test -d $(DESTDIR)$(man1dir) \
		|| $(INSTALL_DIR) $(DESTDIR)$(man1dir)
	test -d $(DESTDIR)$(etcdir) \
		|| $(INSTALL_DIR) $(DESTDIR)$(etcdir)
	test -d $(DESTDIR)$(docdir) \
		|| $(INSTALL_DIR) $(DESTDIR)$(docdir)

.PHONY: install install-strip
install install-strip: build installdirs
	$(INSTALL_PROGRAM) $(PACKAGE).pl $(DESTDIR)$(bindir)/$(PACKAGE)
	$(INSTALL_DATA) $(PACKAGE).1 $(DESTDIR)$(man1dir)/
	$(INSTALL_DATA) config/$(PACKAGE).rc $(DESTDIR)$(etcdir)/
	$(INSTALL_DATA) README NEWS TODO $(DESTDIR)$(docdir)/

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(docdir)/TODO
	rm -f $(DESTDIR)$(docdir)/NEWS
	rm -f $(DESTDIR)$(docdir)/README
	rm -f $(DESTDIR)$(etcdir)/$(PACKAGE).rc
	rm -f $(DESTDIR)$(man1dir)/$(PACKAGE).1
	rm -f $(DESTDIR)$(bindir)/$(PACKAGE)

.PHONY: clean distclean mostlyclean maintainer-clean
clean distclean mostlyclean maintainer-clean:
	rm -f $(PACKAGE).1

.PHONY: dist
dist:
	git archive --format=tar --prefix=$(PACKAGE)-$(VER)/ HEAD \
		| gzip --best > ../$(PACKAGE)-$(VER).tar.gz

.PHONY: check
check: build
	test -f $(PACKAGE).1
