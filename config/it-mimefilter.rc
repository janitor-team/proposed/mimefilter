### This is plain Perl code                       -*- mode: perl -*-

### Dear user,
###
### this configuration is admittedly very hackish, and should be simplified.
### No doubt I will do it some day, but it's not on top of my todo list.
### Feel free to improve it, and send me your patches back!
###
### Thanks, DGMS.

# Our address: this should ultimately be $ENV{'listreq'}.
$::from = "Il gestore di $ENV{'list'} <$ENV{'listreq'}>";

# List of admissible MIME types -- beside "multipart/*" and "message/*", which
# both act as containers (don't ever even think to list "multipart/something"
# and/or "message/something" here: that would be a VERY BAD idea).
#
# Each item in the list may be a regexp.  The regexps are applied anchored with
# "^" and "$" added in front and back, so they should match the entire MIME
# type token, not just some substring.
#
# Parts of a type listed here will be accepted; you probably want to have
# at least '/text/plain', as a minimum.
@::ammessi = (
	      'text/plain',
	      'application/pgp-signature',
	      # 'foo/.*',
	      # '.*bar/foo',
	      );

# List of inadmissibile MIME types.
#
# Each item in the list may be a regexp.  The regexps are applied anchored with
# "^" and "$" added in front and back, so they should match the entire MIME
# type token, not just some substring.
#
# Parts of a type listed here will never be accepted, even if they match some
# regexp listed in @::ammessi.
#
# This list may be used to easily complement @::ammessi, but you should bear in
# mind that if a MIME type doesn't match some regexp in @::ammessi it won't be
# accepted in any way, even if it isn't listed here.  To be more clear, you do
# not need to list here every conceivable MIME type that you don't want in your
# messages, rather just the ones (if any) that would be otherwise accepted by
# some item in your @::ammessi list.
@::nonammessi = (
		 );

### The following here documents get included in our messages if the conditions
### hereby listed happen.

# TO.......: Author of the original message, as seen from (in decreasing order
#            of priority) Reply-To, From or Return-Path.
# CONDITION: MIME parsing of the original message has failed; this is very
#            very unlikely, but possible nevertheless.
@::invalido = <<'EOF;';
Ciao,

questa risposta automatica ti è stata inviata per avvertirti che il tuo
messaggio

		      NON È STATO INVIATO IN LISTA

perché era tecnicamente non corretto (ovvero non rispondente allo
standard MIME).


Questo probabilmente significa che il programma che hai usato per
mandarlo ha avuto qualche problema di funzionamento ed ha generato un
messaggio tecnicamente non valido.


Questo è un problema molto raro: se non sai cosa possa essere successo e
questo problema persiste, se trovi qualcosa che non ti è chiaro o hai
domande, scrivi pure al moderatore della lista, replicando a questo
messaggio.
EOF;

# TO.......: Author of the original message, as seen from (in decreasing order
#            of priority) Reply-To, From or Return-Path.
# CONDITION: Filtering resulted in an empty message (i.e., it was empty, or it
#            has become empty after filtering away unaccettable MIME parts).
@::vuoto = <<'EOF;';
Ciao,

questa risposta automatica ti è stata inviata per avvertirti che il tuo
messaggio

		      NON È STATO INVIATO IN LISTA

perché è risultato vuoto dopo aver eventualmente automaticamente
eliminato le parti non testuali (puoi controllare in fondo a questo
messaggio la lista delle parti eventualmente eliminate).


Le cause più comuni di questo problema sono:

  * Il tuo messaggio era vuoto; in questo caso si sarà probabilmente
    trattato di un errore: non è possibile inviare messaggi vuoti in
    lista (un messaggio è vuoto se il suo corpo non contiene almeno una
    linea che non sia vuota).

  * Il tuo programma di posta elettronica è configurato in modo da
    inviare il messaggio in HTML (il linguaggio usato per la scrittura
    delle pagine Web); in questo caso devi cambiare le impostazioni
    opportune per fargli generare messaggi in formato testo.

  * Era tua intenzione inviare in lista un allegato, per esempio un
    programma, un documento o una immagine: non è possibile inviare
    materiale di questo tipo direttamente in lista.

    Se pensi di aver trovato qualcosa che possa interessare molte delle
    persone iscritte in lista, mettiti prima in contatto col moderatore:
    ti spiegherà come fare per metterlo a disposizione di tutti senza
    bisogno di inviarlo in lista.


Se trovi qualcosa che non ti è chiaro o hai domande, scrivi pure al
moderatore della lista, replicando a questo messaggio.
EOF;

# TO.......: Author of the original message, as seen from (in decreasing order
#            of priority) Reply-To, From or Return-Path.
# CONDITION: Some MIME parts have been stripped from the original message
#            before letting it through.
@::mutilato = <<'EOF;';
Ciao,

questa risposta automatica ti è stata inviata per avvertirti che anche
se il tuo messaggio

		È STATO CORRETTAMENTE INVIATO IN LISTA,

alcune delle parti non testuali che esso conteneva sono state tagliate
prima che esso fosse inoltrato.

	       NON HAI BISOGNO DI RIMANDARE IL MESSAGGIO.


Le cause più comuni di questo problema sono:

  * Il tuo programma di posta elettronica è configurato in modo da
    inviare nello stesso messaggio due copie del testo che scrivi: una
    pura e semplice e una in formato HTML (il linguaggio usato per la
    scrittura delle pagine Web).

    Il programma "Microsoft Outlook Express", per esempio, è impostato
    così dalla fabbrica: se questo è il tuo caso, puoi eliminare il
    problema seguendo, nel menù, il percorso:
        strumenti -> opzioni -> invio -> "testo normale"
    e premendo quindi "applica" e "ok";

  * Era tua intenzione inviare in lista un allegato, per esempio un
    programma, un documento o una immagine: non è possibile inviare
    materiale di questo tipo direttamente in lista.

    Se pensi di aver trovato qualcosa che possa interessare molte delle
    persone iscritte in lista, mettiti prima in contatto col moderatore:
    ti spiegherà come fare per metterlo a disposizione di tutti senza
    bisogno di inviarlo in lista.


Se trovi qualcosa che non ti è chiaro o hai domande, scrivi pure al
moderatore della lista, replicando a questo messaggio.
EOF;

# TO.......: Author of the original message, as seen from (in decreasing order
#            of priority) Reply-To, From or Return-Path.
# CONDITION: Added to the bottom of our message to the author if there has
#            been any stripped part.  A complete list of every stripped part
#            will be added to this message.
@::tagliato = <<'EOF;';

Ecco una lista dei tipi MIME di ciascuna parte tagliata:
EOF;

### When we strip some part, we replace it with a short text explaining what
### was there.

# MIME descriptions of part and message replacements: think of it as a title.
$::descrizione = 'AVVERTIMENTO AUTOMATICO';

# This text replaces a stripped part (W: _TIPOMIME_ is a KEYWORD).
@::sostituto = <<"EOF;";
Qui era presente una parte di messaggio non ammessa di tipo \'_TIPOMIME_\'
che è stata automaticamente eliminata prima dell\'invio in lista.
EOF;

# This text replaces a stripped message (W: _TIPOMIME_ is a KEYWORD).
# Embedded messages are discarded if, and only if, they are or become empty.
# If the top most message entity becomes empty, nothing will get through.
@::messaggio = <<"EOF;";
Qui era presente un messaggio annidato di tipo \'_TIPOMIME_\' che è stato
automaticamente eliminato prima dell\'invio in lista perché privo di
parti testuali.
EOF;
